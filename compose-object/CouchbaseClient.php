<?php
class CouchbaseClient {
	private $_cb;

	public function __construct($cb){
		$this->_cb = $cb;

	}

	public function view($view_desing, $view_name, $options = array()) {
		$string = $view_desing . " | " . $view_name . " | " . json_encode($options);
		try {
			return $this->_cb->view($view_desing, $view_name, $options);
		}
		catch(Exception $e) {
			$date = date('Ymd');
			$code = $e->getCode();
			$message = $e->getMessage();
			file_put_contents('/tmp/CouchbaseExceptionView', $date . $string. ' | '.$code. ' | '.$message."\n",FILE_APPEND);
			throw $e;
		}		
	}

	public function getMulti($id, &$cas, $options) {
		return $this->_cb->getMulti($id, $cas, $options);
	
	}

	public function __call($name, $arguments) {
		try { 
			$metodoReflexionado = new ReflectionMethod(get_class($this->_cb), $name);
			return $metodoReflexionado->invokeArgs($this->_cb, $arguments) ;
		} catch (Exception $e) {
			throw new Exception("Error Processing Request", 1);
		}
	}	
}
