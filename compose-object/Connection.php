<?php
require_once(__DIR__.'/Singleton.php');
require_once(__DIR__.'/CouchbaseClient.php');

class CouchbaseConnection extends Singleton
{
    public function getConnection($buket) {
        $couchbase = option('couchbase');
        return new CouchbaseClient(new Couchbase($couchbase['host'], $couchbase['user'], $couchbase['password'], $buket));
    }
}
